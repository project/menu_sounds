Menu Sounds
===========

Menu Sounds lets you add sounds to menu items, because who needs flash to make
annoying websites.

After enabling the module you can add audio files to every menu item, and choose
when to play those files (when clicking or when hovering over the menu link).
Just to be sure, this means that whenever a user hovers (or clicks) your menu
links he / she will hear a sound. If your site users hate you don't blame me!

Jokes aside, I think there are still valid uses for this or I wouldn't be
releasing it.

Browser support
---------------

The sound is played using the html5 audio element, so only Firefox 3.5+,
Chrome 3+, Opera 10.5+, Safari 4+ and IE 9+ are supported.

To maximize browser support two files are expected:
* mp3 for Webkit and IE
* ogg for Firefox and Opera

Installation
------------

As always, download, extract and enable, or just
    $ drush dl menu_sounds
    $ drush en menu_sounds

Dependencies
------------

* menu (core)
* file (core)
* https://drupal.org/project/ctools

Credits
-------

This is based heavily on http://css-tricks.com/play-sound-on-hover/
