(function ($) {
  /**
   * Play sounds associated with menu items.
   */
  Drupal.behaviors.menuSounds = {
    attach: function (context, settings) {
      $('.menu-sounds-has-audio').once('menu_sounds', function() {
        var link = $(this).children('a');
        var audio = link.children('.menu-sounds-audio');
        var ev = audio.data('event');
        if (ev == 'hover') {
          link.mouseenter(function() {
            audio[0].play();
          });
        }
        else if (ev == 'click') {
          link.click(function() {
              audio[0].play();
          });
        }
      });
    }
  };
})(jQuery);
